

#include <assert.h>
#include <errno.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


#define PAGE_SIZE 4096
struct test_info {
    bool *status;
    size_t *capacities;
    size_t length;
};



void check_heap(void *heap, struct test_info old_info, size_t addLast) {

    size_t nCapacities[old_info.length + 1];
    bool nStatus[old_info.length + 1];

    size_t lastValue = addLast + REGION_MIN_SIZE - offsetof(struct block_header, contents) * (old_info.length + 1);

    for (size_t i=0; i < old_info.length;i++){
        nCapacities[i] = old_info.capacities[i];
        nStatus[i] = old_info.status[i];
        lastValue -= old_info.capacities[i];
    }

    nStatus[old_info.length] = true;
    nCapacities[old_info.length] = lastValue;

    struct test_info info = {
            .length = old_info.length + 1,
            .status = nStatus,
            .capacities = nCapacities
    };


    size_t i = -1;
    struct block_header *current = heap;
    while (current != NULL) {
        i++;
        assert(i < info.length);
        assert(info.status[i] == current->is_free);
        assert(info.capacities[i] == current->capacity.bytes);
        current = current->next;
    }
}


void single_block_test() {
    void *heap = heap_init(0);
    _malloc(200);

    size_t block_size = 1;

    check_heap(
            heap,
            (struct test_info) {
                    (bool[]) {false},
                    (size_t[]) {200},
                    block_size
            }, 0
    );
    heap_term();
}


void single_block_free_test() {
    void *heap = heap_init(0);
    _malloc(200);
    void* block = _malloc(300);
    _malloc(400);
    _malloc(500);

    _free(block);

    size_t block_size = 4;

    check_heap(
            heap,
            (struct test_info) {
                    (bool[]) {false, true, false, false},
                    (size_t[]) {200, 300, 400, 500},
                    block_size
            }, 0
    );
    heap_term();
}

void few_block_free_test() {
    void *heap = heap_init(0);
    _malloc(200);
    void* block1 = _malloc(300);
    _malloc(400);
    void* block2 = _malloc(500);

    _free(block1);
    _free(block2);


    check_heap(
            heap,
            (struct test_info) {
                    (bool[]) {false, true, false}, //последний блок будет объединён с остатком
                    (size_t[]) {200, 300, 400},
                    3
            }, 0
    );
    heap_term();
}

void more_memory_test() {
    void *heap = heap_init(0);
    _malloc(200);
    _malloc(REGION_MIN_SIZE);

    check_heap(
            heap,
            (struct test_info) {
                    (bool[]) { false, false}, //последний блок будет объединён с остатком
                    (size_t[]) {200, REGION_MIN_SIZE},
                    2
            }, 12288 //отдельно посчитать с учётом round page

    );
    heap_term();
}

void more_memory_another_place_test(){
    void *heap = heap_init(0);
    map_pages(HEAP_START + REGION_MIN_SIZE,
                1024,
                                   1048576);
    assert(errno == 0);
    _malloc(200);
    void* big_part = _malloc(REGION_MIN_SIZE - 100);

    check_heap(
            heap,
            (struct test_info) {
                    (bool[]) { false,true, false}, //последний блок будет объединён с остатком
                    (size_t[]) {200, REGION_MIN_SIZE - 200 - 2 * offsetof(struct block_header, contents), REGION_MIN_SIZE - 100},
                    3
            }, 8192 //отдельно посчитать с учётом round page

    );
    assert(big_part - HEAP_START > REGION_MIN_SIZE || big_part < HEAP_START); // проверка того, что блок находится в "другом месте"
    heap_term();
}

int main() {
    single_block_test();
    single_block_free_test();
    few_block_free_test();
    more_memory_test();
    more_memory_another_place_test();

    printf("Success\n");
    return 0;
}
